package uebung3

class Person {

	String vorname
	String nachname
	Date geburtsdatum
	List<Adresse> adressen
	
    static constraints = {
		vorname blank:false, nullable : false
		nachname blank:false, nullable : false
		geburtsdatum nullable : false
    }
	
	static hasMany = [adressen : Adresse]
}
