package uebung3

class Adresse {

	String strasse
	int hausnummer
	String postleihzahl
	String stadt
	
    static constraints = {
		strasse nullable:false, maxLength : 200, blank : false
		hausnummer nullable: false, min : 1 
		postleihzahl nullable: false, blank : false, matches : /\d{5}/ 
		stadt nullable: false, blank : false, maxLength : 100
    }
}
